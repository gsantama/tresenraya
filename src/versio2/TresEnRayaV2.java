package versio2;

import java.util.Scanner;

public class TresEnRayaV2{
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int opcio = -1;

		System.out.println("\n**********Bienvenido al juego del 3 en Raya**********");

		while (opcio != 0) {
			opcio = menu();
			switch (opcio) {
			case 1:
				ayuda();
				break;
			case 2:
				Jugadors.jugador1 = user();
				Jugadors.jugador2 = user2();
				break;
			case 3:
				jugarPartida();
				//jugarPartida(tablero, nombre1, nombre2);
				break;
			case 4:
				System.out.println("Jugador 1: " + Jugadors.jugador1 + " ha guanyat " + Jugadors.guanyadesJ1);
				System.out.println("Jugador 2: " + Jugadors.jugador2 + " ha guanyat " + Jugadors.guanyadesJ2);
				break;
			case 0:
				System.out.println("Adios");
				break;
			default:
				System.err.println("Opcion invalida");

			}
		}

	}

	private static int menu() {

		int option;
		System.out.println();
		System.out.println();
		System.out.println("Selecciona una opcion");
		System.out.println();
		System.out.println();
		System.out.println("Opcion 1: Ayuda");
		System.out.println("Opcion 2: Definir Jugador");
		System.out.println("Opcion 3: Jugar Partida");
		System.out.println("Opcion 4: Ver Jugador");
		System.out.println("Opcion 0: Exit");

		option = sc.nextInt();
		do {

		} while (option < 0 && option > 4); // recuerda poner el AND && Porque si pones el || seria o mas grande que 0 o
											// m�s peque que 4

		return option;

	}

	private static void ayuda() { // opcion "ayuda", es void porque no devuelve nada

		System.out.println("--> El jugador 1 ser� una 'X' el otro jugador una 'O'");
		System.out.println("--> El juego trata de conseguir una raya de 3 casillas antes que el otro");
		System.out.println("--> La raya puede ser vertical, horizontal o diagonal");
	}

	private static String user() {// trataremos el String del jugador y es lo que devolveremos

		System.out.println("Nombre del Jugador 1: ");
		String jugador1 = sc.next();

		return jugador1;

	}

	private static String user2() {// trataremos el String del jugador2 y es lo que devolveremos

		System.out.println("Nombre del Jugador 2: ");
		String jugador2 = sc.next();

		return jugador2;

	}

	private static void inicialitzarTauler(String[][] tablero) {
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				tablero[i][j] = "�";
			}
		}
	}

	private static void visualitzarTauler(String[][] tablero) {
		for (int i = 0; i < tablero.length; i++) {
			System.out.print(i + " "); // para numerar las filas
			for (int j = 0; j < tablero.length; j++) {
				System.out.print(tablero[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println("  0 1 2");
	}

	static int obtenirFila() { //cal garantir que sigui un valor entre 0 i 2
		int fila;
		do {
			System.out.println(" dime una fila: ");
			fila = sc.nextInt();
		}while (fila<0 || fila >2);
		return fila;
	}

	static int obtenirColumna() {   //cal garantir que sigui un valor entre 0 i 2
		int columna;
		do {
			System.out.println(" dime una columna: ");
			columna = sc.nextInt();
		}while(columna<0 || columna>2);
		return columna;
	}

	static boolean lliure(String [][]tablero, int f, int c) {
		return (tablero[f][c] == "�");
	}

	static void marcar(String[][]tablero, int f,int c, boolean torn) {
		if (torn) 
			tablero[f][c]="O";
		else
			tablero[f][c]="X";
	}

	static boolean comprovar(String[][]tablero) {
		boolean ganar = false;
		
		ganar = ganar || ((tablero[0][0] == tablero[0][1]) && (tablero[0][1] == tablero[0][2])  && tablero[0][0]!="�") ;
		ganar = ganar || ((tablero[1][0] == tablero[1][1]) && (tablero[1][1] == tablero[1][2])  && tablero[1][0]!="�") ;
		ganar = ganar || ((tablero[2][0] == tablero[2][1]) && (tablero[2][1] == tablero[2][2])  && tablero[2][0]!="�") ;
		
		ganar = ganar || ((tablero[0][0] == tablero[1][0]) && (tablero[1][0] == tablero[2][0])  && tablero[0][0]!="�") ;
		ganar = ganar || ((tablero[0][1] == tablero[1][1]) && (tablero[1][1] == tablero[2][1])  && tablero[0][1]!="�") ;
		ganar = ganar || ((tablero[0][2] == tablero[1][2]) && (tablero[1][2] == tablero[2][2])  && tablero[0][2]!="�") ;
		
		ganar = ganar || ((tablero[0][0] == tablero[1][1]) && (tablero[1][1] == tablero[2][2])  && tablero[0][0]!="�") ;
		ganar = ganar || ((tablero[0][2] == tablero[1][1]) && (tablero[1][1] == tablero[2][0])  && tablero[0][2]!="�") ;
		
		 return ganar;
		}
	
	
	private static void jugarPartida() {
		String[][] tablero = new String [3][3];
		
		inicialitzarTauler(tablero);
		visualitzarTauler(tablero);
		
		boolean ganar = false;  //hi ha guanyador o no?
		boolean torn = false;   //torn false: jugador1, true jugador2
		int fila;	//fila de casella que vull marcar
		int col;	// columna de la casella que vull marcar
		int contador = 0;  //n�mero de jugades fetes

		// mentre no ni ha guanyador i queden jugades per fer
		// guanyador ho controlem per la varible ganar
		// queden jugades ho controlem per la variable contador
		
		while (!ganar && contador < 9) {
				//triem una caselle del tauler
				fila =  obtenirFila();
				col =  obtenirColumna();

				if (lliure(tablero, fila, col)) { // este if es para que escoja una fila/columna que no haya sido  elegida anteriormente
						marcar(tablero, fila, col,torn);
						visualitzarTauler(tablero);
						ganar = comprovar(tablero);
						
						if(!ganar) { 	//si no hi ha guanyador
							torn = !torn;	// canviem el torn
							contador ++;	//augmentem el n�mero de jugades
						}
				}
				else 
					System.out.println("Casella ocupada");
		}
		
		if(ganar) {
			if (torn) {
				System.err.println("Ganador " + Jugadors.jugador2);
				Jugadors.guanyadesJ2 ++;
			}
			else {
				System.err.println("Ganador " + Jugadors.jugador1);
				Jugadors.guanyadesJ1 ++;
			}
		}
		else
			System.err.println("No hi ha guanyador");
	}
}